<?php

namespace App\Repository;

use App\Entity\CaseItems;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CaseItems|null find($id, $lockMode = null, $lockVersion = null)
 * @method CaseItems|null findOneBy(array $criteria, array $orderBy = null)
 * @method CaseItems[]    findAll()
 * @method CaseItems[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CaseItemsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CaseItems::class);
    }

    // /**
    //  * @return CaseItems[] Returns an array of CaseItems objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    // /**
    //  * @return CaseItems[] Returns an array of CaseItems objects
    //  */
    public function findAny(string $search)
    {
        return $this->createQueryBuilder('c')
        ->andWhere('c.Name = :search')
        ->setParameter('search', $search)
        ->getQuery()
        ->getResult();
    }
    /*
    public function findOneBySomeField($value): ?CaseItems
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
