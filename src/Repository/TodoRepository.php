<?php

namespace App\Repository;

use App\Entity\Todo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Todo|null find($id, $lockMode = null, $lockVersion = null)
 * @method Todo|null findOneBy(array $criteria, array $orderBy = null)
 * @method Todo[]    findAll()
 * @method Todo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TodoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Todo::class);
    }

    // /**
    //  * @return Todo[] Returns an array of Todo objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    public function findbyCases($cases, $currentdate)
    {
        $qb = $this->createQueryBuilder('i')
        ->Where('i.CaseID IN (:cases)')
        ->andwhere('i.StartDate >= :current')
        ->setParameter('cases', $cases)
        ->setParameter('current', $currentdate)
        ->orderBy('i.StartDate', 'ASC');

        return $qb->getQuery()->getResult();
    }

    public function findByCasesBetween($cases, $currentdate)
    {
        $qb = $this->createQueryBuilder('i')
        ->Where('i.CaseID IN (:cases)')
        ->andwhere('i.StartDate >= :current')
        ->andwhere('i.StartDate <= :plusmonth')
        ->setParameter('cases', $cases)
        ->setParameter('current', $currentdate)
        ->setParameter('plusmonth', mktime(0, 0, 0, date("m",$currentdate)+1, 0, date("Y")))
        ->orderBy('i.StartDate', 'ASC');

        return $qb->getQuery()->getResult();
    }

    public function findByCasesNoDate($cases)
    {
        $qb = $this->createQueryBuilder('i')
        ->Where('i.CaseID IN (:cases)')
        ->setParameter('cases', $cases)
        ->orderBy('i.StartDate', 'ASC');

        return $qb->getQuery()->getResult();
    }

    // /**
    //  * @return Todo[] Returns an array of Todo objects
    //  */
    public function findAny(string $search)
    {
        return $this->createQueryBuilder('c')
        ->andWhere('c.Name LIKE :search')
        ->setParameter('search', '%'.$search.'%')
        ->getQuery()
        ->getResult();
    }
    /*
    public function findOneBySomeField($value): ?Todo
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
