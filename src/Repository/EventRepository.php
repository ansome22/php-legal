<?php

namespace App\Repository;

use App\Entity\Event;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Event|null find($id, $lockMode = null, $lockVersion = null)
 * @method Event|null findOneBy(array $criteria, array $orderBy = null)
 * @method Event[]    findAll()
 * @method Event[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EventRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Event::class);
    }

    // /**
    //  * @return Event[] Returns an array of Event objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    // /**
    //  * @return Event[] Returns an array of Event objects
    //  */
    public function findAny(string $search)
    {
        return $this->createQueryBuilder('c')
        ->andWhere('c.Name LIKE :search')
        ->setParameter('search', '%'.$search.'%')
        ->getQuery()
        ->getResult();
    }


    public function findbyCases($cases, $currentdate)
    {
        $qb = $this->createQueryBuilder('i')
        ->Where('i.CaseID IN (:cases)')
        ->andwhere('i.StartDate >= :current')
        ->setParameter('cases', $cases)
        ->setParameter('current', $currentdate)
        ->orderBy('i.StartDate', 'ASC');

        return $qb->getQuery()->getResult();
    }

    public function findByCasesBetween($cases, $currentdate)
    {
        $qb = $this->createQueryBuilder('i')
        ->Where('i.CaseID IN (:cases)')
        ->andwhere('i.StartDate >= :current')
        #->andwhere('i.StartDate <= :plusmonth')
        ->setParameter('cases', $cases)
        ->setParameter('current', $currentdate)
        #->setParameter('plusmonth', mktime(0, 0, 0, date("m",$currentdate)+1, 0, date("Y")))
        ->orderBy('i.StartDate', 'ASC');

        return $qb->getQuery()->getResult();
    }

    public function findOneByID($value): ?Event
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.id = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    
}
