<?php

namespace App\Repository;

use App\Entity\Invoice;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Invoice|null find($id, $lockMode = null, $lockVersion = null)
 * @method Invoice|null findOneBy(array $criteria, array $orderBy = null)
 * @method Invoice[]    findAll()
 * @method Invoice[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InvoiceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Invoice::class);
    }

    // /**
    //  * @return Invoice[] Returns an array of Invoice objects
    //  */
    
    public function findByCases($cases)
    {
        $qb = $this->createQueryBuilder('i')
        ->Where('i.cases IN (:cases)')
        ->setParameter('cases', $cases);
        return $qb->getQuery()->getResult();
    }
    
    // /**
    //  * @return Invoice[] Returns an array of Invoice objects
    //  */
    public function findAny(string $search)
    {
        return $this->createQueryBuilder('c')
        ->andWhere('c.Name LIKE :search')
        ->setParameter('search', '%'.$search.'%')
        ->getQuery()
        ->getResult();
    }
    /*
    public function findOneBySomeField($value): ?Invoice
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
