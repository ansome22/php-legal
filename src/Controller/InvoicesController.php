<?php

namespace App\Controller;

use App\Entity\Invoice;
use App\Entity\Cases;
use App\Entity\Client;
use App\Entity\InvoiceItems;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

use Knp\Snappy\Pdf;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;

use App\Form\InvoiceItemsFormType;
use App\Form\InvoiceFormType;

class InvoicesController extends AbstractController
{
    /**
     * @Route("/invoices", name="invoices")
     */
    #[Route('/invoices', name: 'invoices')]
    public function index(): Response
    {
        $user = $this->getUser();
        $cases = $user->getStaff()->getCases();

        $invoices = $this->getDoctrine()->getRepository(Invoice::class)->findByCases($cases);

        return $this->render('invoices/index.html.twig', [
            'invoices' => $invoices
        ]);
    }
    /**
     * @Route("/invoice/new", name="invoiceNew")
     */
    #[Route('/invoice/new', name: 'invoiceNew')]
    public function new(Request $request): Response
    {
        $user = $this->getUser();
        $entityManager = $this->getDoctrine()->getManager();
        $invoice = new Invoice();
        $form = $this->createForm(InvoiceFormType::class, $invoice);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $event = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($event);
            $entityManager->flush();

            return $this->redirectToRoute('invoices');
        }
        else
        {
            return $this->render('invoices/new.html.twig', [
                'invoice' => $invoice,
                'case_form' => $form->createView()
            ]);
        }
    }
    /**
     * @Route("/invoice/{id}", name="invoiceShow")
     */
    #[Route('/invoice/{id}', name: 'invoiceShow')]
    public function show(string $id): Response
    {
        $user = $this->getUser();
        $entityManager = $this->getDoctrine()->getManager();
        $invoice = $entityManager->getRepository(Invoice::class)->find($id);
        $invoiceitems = $entityManager->getRepository(InvoiceItems::class)->findByInvoiceID($invoice->getId());

        return $this->render('invoices/show.html.twig', [
            'invoice' => $invoice,
            'invoiceitems'=> $invoiceitems
        ]);
    }
    /**
     * @Route("/invoice/{id}/edit", name="invoiceEdit")
     */
    #[Route('/invoice/{id}/edit', name: 'invoiceEdit')]
    public function edit(string $id,Request $request): Response
    {
        $user = $this->getUser();
        $entityManager = $this->getDoctrine()->getManager();
        $invoice = $entityManager->getRepository(Invoice::class)->find($id);
        $form = $this->createForm(InvoiceFormType::class, $invoice);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $event = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($event);
            $entityManager->flush();

            return $this->redirectToRoute('invoiceShow',array('id' => $id));
        }
        else
        {
            return $this->render('invoices/edit.html.twig', [
                'invoice' => $invoice,
                'case_form' => $form->createView()
            ]);
        }
    }
    /**
     * @Route("/invoice/{id}/print", name="invoicePrint")
     */
    #[Route('/invoice/{id}/print', name: 'invoicePrint')]
    public function print(string $id, Pdf $Pdf): Response
    {
        $user = $this->getUser();
        $entityManager = $this->getDoctrine()->getManager();
        $invoice = $entityManager->getRepository(Invoice::class)->find($id);
        $clientID = $invoice->getCases()->getClientID();
        $client = $entityManager->getRepository(Client::class)->findOneByID($clientID->getId());
        $invoiceitems = $entityManager->getRepository(InvoiceItems::class)->findByInvoiceID($invoice->getId());
        $html = $this->renderView('invoices/print.html.twig', [
            'invoice' => $invoice,
            'invoiceitems'=> $invoiceitems,
            'client'=> $client,
        ]);

        return new PdfResponse(
            $Pdf->getOutputFromHtml($html),
            'invoice.pdf'
        );
    }
    /**
     * @Route("/invoice/{invoiceid}/item/new", name="invoiceItemNew")
     */
    #[Route('/invoice/{invoiceid}/item/new', name: 'invoiceItemNew')]
    public function itemNew(string $invoiceid,Request $request): Response
    {
        $user = $this->getUser();
        $entityManager = $this->getDoctrine()->getManager();
        $invoice = $entityManager->getRepository(InvoiceItems::class)->find($invoiceid);
        $form = $this->createForm(InvoiceItemsFormType::class, $invoice);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $event = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($event);
            $entityManager->flush();

            return $this->redirectToRoute('invoiceShow',array('id' => $invoiceid));
        }
        else
        {
            return $this->render('invoices/items/add.html.twig', [
                'invoice' => $invoice,
                'case_form' => $form->createView()
            ]);
        }
    }

    
    /**
     * @Route("/invoice/{invoiceid}/item/{id}/edit", name="invoiceItemEdit")
     */
    #[Route('/invoice/{invoiceid}/item/{id}/edit', name: 'invoiceItemEdit')]
    public function Itemedit(string $invoiceid,string $id,Request $request): Response
    {
        $user = $this->getUser();
        $entityManager = $this->getDoctrine()->getManager();
        $invoice = $entityManager->getRepository(InvoiceItems::class)->find($id);
        $form = $this->createForm(InvoiceItemsFormType::class, $invoice);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $event = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($event);
            $entityManager->flush();

            return $this->redirectToRoute('invoiceShow',array('id' => $invoiceid));
        }
        else
        {
            return $this->render('invoices/items/edit.html.twig', [
                'invoice' => $invoice,
                'case_form' => $form->createView()
            ]);
        }
    }
    /**
     * @Route("/invoice/{invoiceid}/item/{id}/remove", name="invoiceItemRemove")
     */
    #[Route('/invoice/{invoiceid}/item/{id}/remove', name: 'invoiceItemRemove')]
    public function Itemremove(string $invoiceid,string $id): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $event = $entityManager->getRepository(Event::class)->find($id);
        $entityManager->remove($event);
        $entityManager->flush();

        return $this->redirectToRoute('invoiceShow',array('id' => $invoiceid));
    }
    /**
     * @Route("/invoice/{id}/remove", name="invoiceRemove")
     */
    #[Route('/invoice/{id}/remove', name: 'invoiceRemove')]
    public function remove(string $id): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $event = $entityManager->getRepository(Event::class)->find($id);
        $entityManager->remove($event);
        $entityManager->flush();

        return $this->redirectToRoute('dashboard');
    }
}
