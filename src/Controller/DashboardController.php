<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

class DashboardController extends AbstractController
{
    /**
     * @Route("/dashboard", name="dashboard")
     */
    #[Route('/dashboard', name: 'dashboard')]
    public function index(Environment $twig): Response
    {
        $user = $this->getUser();

        $staff = $user->getStaff();

        $cases = $staff->getCases();

        return new Response($twig->render('dashboard/index.html.twig', [
            'controller_name' => 'DashboardController',
            'StaffName'=> $staff->getFname(),
            'cases'=> $cases,
            'todos'=> $staff->getTodos(),
            'invoices'=> $staff->getInvoices(),
            'current', getdate()
        ]));
    }
}
