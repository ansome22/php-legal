<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Twig\Environment;


class InitController extends AbstractController
{
    /**
     * @Route("/", name="Home")
     */
    #[Route('/', name: 'Home')]
    public function index(Environment $twig): Response
    {
        return new Response($twig->render('General/index.html.twig', [
                       
                    ]));
    }
}
