<?php

namespace App\Controller;

use App\Entity\Event;
use App\Entity\Todo;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\EventFormType;
use Symfony\Component\HttpFoundation\Request;

class CalendarController extends AbstractController
{
    /**
     * @Route("/calendar", name="calendar")
     */
    #[Route('/calendar', name: 'calendar')]
    public function index(): Response
    {
        #string $month,string $year
        $user = $this->getUser();
        $cases = $user->getStaff()->getCases();

        $currentdate = time();


        $events = $this->getDoctrine()
        ->getRepository(Event::class)
        ->findByCases($cases,date("Ymd",$currentdate));

        $todo =$this->getDoctrine()
        ->getRepository(Todo::class)
        ->findByCases($cases,$currentdate);
        
        return $this->render('calendar/index.html.twig', [
            'events' => $events,
            'todos' => $todo,
            'current'=> date("Ymd",$currentdate)
        ]);


    }

    /**
     * @Route("/calendar/event/new", name="eventAdd")
     */
    #[Route('/calendar/event/new', name: 'eventAdd')]
    public function add(Request $request): Response
    {
        $user = $this->getUser();
        $event = new Event();
        $form = $this->createForm(EventFormType::class, $event);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $event = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($event);
            $entityManager->flush();

            return $this->redirectToRoute('calendar');
        }
        else
        {
            return $this->render('calendar/new.html.twig', [
                'controller_name' => 'CalendarController',
                'case_form' => $form->createView(),
            ]);
        }


    }

    /**
     * @Route("/calendar/event/{id}", name="eventShow")
     */
    #[Route('/calendar/event/{id}', name: 'eventShow')]
    public function EventShow(int $id): Response
    {
        $user = $this->getUser();
        $event = $this->getDoctrine()
        ->getRepository(Event::class)
        ->find($id);


            return $this->render('calendar/show.html.twig', [
                'controller_name' => 'CalendarController',
                'event' => $event,
            ]);


    }
    /**
     * @Route("/calendar/event/{id}/edit", name="eventEdit")
     */
    #[Route('/calendar/event/{id}/edit', name: 'eventEdit')]
    public function Eventedit(int $id,Request $request): Response
    {
        $user = $this->getUser();
        $event = $this->getDoctrine()
        ->getRepository(Event::class)->find($id);
        $form = $this->createForm(EventFormType::class, $event);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $event = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($event);
            $entityManager->flush();

            return $this->redirectToRoute('eventShow',array('id' => $id));
        }
        else
        {
            return $this->render('calendar/new.html.twig', [
                'controller_name' => 'CalendarController',
                'case_form' => $form->createView(),
            ]);
        }

    }


    /**
     * @Route("/calendar/event/{id}/remove", name="eventRemove")
     */
    #[Route('/calendar/event/{id}/remove', name: 'eventRemove')]
    public function Eventremove(int $id, Request $request): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $event = $entityManager->getRepository(Event::class)->find($id);
        $entityManager->remove($event);
        $entityManager->flush();

        return $this->redirectToRoute('calendar');
    }

}
