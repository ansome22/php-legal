<?php

namespace App\Controller;

use App\Entity\Template;
use App\Form\TemplateFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Service\FileUploader;


class TemplatesController extends AbstractController
{
        /**
     * @Route("templates", name="templates")
     */
    #[Route('/templates', name: 'templates')]
    public function index(): Response
    {
        $user = $this->getUser();
        $staff = $user->getStaff();

        $templates = $this->getDoctrine()
        ->getRepository(Template::class)
        ->findAll();

        
        return $this->render('templates/index.html.twig', [
            'templates' => $templates,
            'staff' => $staff
        ]);
    }

    /**
     * @Route("/templates/new", name="templateNew")
     */
    #[Route('/templates/new', name: 'templateNew')]
    public function new(Request $request, FileUploader $fileUploader): Response
    {
        $user = $this->getUser();
        $template = new Template();

        $form = $this->createForm(TemplateFormType::class, $template);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $template = $form->getData();

            /** @var UploadedFile $brochureFile */
            $File = $form->get('File')->getData();
            // this condition is needed because the 'brochure' field is not required
            // so the PDF file must be processed only when a file is uploaded
            if ($File) {
                $FileName = $fileUploader->upload($File);
                $template->setFile($FileName);
            }
            $template->setStaff($user->getStaff());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($template);
            $entityManager->flush();

            return $this->redirectToRoute('templates');
        }
        else
        {
            return $this->render('templates/new.html.twig', [
                'case_form' => $form->createView(),
            ]);
        }
    }

    /**
     * @Route("/templates/{id}", name="templateShow")
     */
    #[Route('/templates/{id}', name: 'templateShow')]
    public function show(string $id,Request $request): Response
    {
        $user = $this->getUser();
        $template = $this->getDoctrine()
        ->getRepository(Template::class)
        ->findOneBy(['id'=>$id]);

        return $this->render('templates/show.html.twig', [
            'template' => $template,
        ]);
    }

        /**
     * @Route("/templates/{id}/edit", name="templateEdit")
     */
    #[Route('/templates/{id}/edit', name: 'templateEdit')]
    public function edit(string $id,Request $request): Response
    {
        $user = $this->getUser();
        $template = $this->getDoctrine()
        ->getRepository(Template::class)
        ->find($id);

        $form = $this->createForm(TemplateFormType::class, $template);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $template = $form->getData();

            /** @var UploadedFile $brochureFile */
            $File = $form->get('File')->getData();
            // this condition is needed because the 'brochure' field is not required
            // so the PDF file must be processed only when a file is uploaded
            if ($File) {
                $FileName = $fileUploader->upload($File);
                $template->setFile($FileName);
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($template);
            $entityManager->flush();

            return $this->redirectToRoute('templateShow',array('id' => $id));
        }
        else
        {
            return $this->render('templates/edit.html.twig', [
                'case_form' => $form->createView(),
            ]);
        }
    }
        /**
     * @Route("/templates/{id}/remove", name="templateRemove")
     */
    #[Route('/templates/{id}/remove', name: 'templateRemove')]
    public function delete(string $id,Request $request): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $event = $entityManager->getRepository(Template::class)->find($id);
        $entityManager->remove($event);
        $entityManager->flush();

        return $this->redirectToRoute('templates');
    }
}
