<?php

namespace App\Controller;

use App\Entity\Todo;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\TodoFormType;
use Symfony\Component\HttpFoundation\Request;

class TodoController extends AbstractController
{
    /**
     * @Route("/todo", name="todos")
     */
    #[Route('/todo', name: 'todos')]
    public function index(): Response
    {
        $user = $this->getUser();
        $cases = $user->getStaff()->getCases();
        $todo = $this->getDoctrine()->getRepository(Todo::class)->findByCasesNoDate($cases);

        return $this->render('todo/index.html.twig', [
            'controller_name' => 'TodoController',
            'todos' => $todo
        ]);
    }

    /**
     * @Route("/todo/new", name="todoNew")
     */
    #[Route('/todo/new', name: 'todoNew')]
    public function new(Request $request): Response
    {
        $user = $this->getUser();
        $todo = new Todo();
        $form = $this->createForm(TodoFormType::class, $todo);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $todo = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($todo);
            $entityManager->flush();

            return $this->redirectToRoute('calendar');
        }
        else
        {
            return $this->render('todo/new.html.twig', [
                'case_form' => $form->createView(),
            ]);

        }


    }

        /**
     * @Route("/todo/{id}/edit", name="todoEdit")
     */
    #[Route('/todo/{id}/edit', name: 'todoEdit')]
    public function edit(string $id,Request $request): Response
    {
        $user = $this->getUser();
        $todo = $this->getDoctrine()
        ->getRepository(Todo::class)
        ->find($id);

        $form = $this->createForm(TodoFormType::class, $todo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $todo = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($todo);
            $entityManager->flush();

            return $this->redirectToRoute('todo',array('id' => $id));
        }
        else
        {
            return $this->render('todo/edit.html.twig', [
                'controller_name' => 'CalendarController',
                'case_form' => $form->createView(),
            ]);
        }
    }

            /**
     * @Route("/todo/{id}/remove", name="todoRemove")
     */
    #[Route('/todo/{id}/remove', name: 'todoRemove')]
    public function remove(string $id): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $event = $entityManager->getRepository(Todo::class)->find($id);
        $entityManager->remove($event);
        $entityManager->flush();

        return $this->redirectToRoute('calendar');
    }

    /**
     * @Route("/todo/{id}", name="todo")
     */
    #[Route('/todo/{id}', name: 'todo')]
    public function show(string $id): Response
    {
        $user = $this->getUser();
        $todo = $this->getDoctrine()
        ->getRepository(Todo::class)
        ->findOneBy(['id'=>$id]);

        return $this->render('todo/show.html.twig', [
            'controller_name' => 'TodoController',
            'todo' => $todo
        ]);
    }
}
