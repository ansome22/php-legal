<?php

namespace App\Controller;

use App\Entity\Cases;
use App\Entity\Event;
use App\Entity\Invoice;
use App\Entity\Staff;
use App\Entity\Client;
use App\Entity\CaseItems;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class SearchController extends AbstractController
{
    /**
     * @Route("/search", name="search")
     */
    #[Route('/search', name: 'search')]
    public function index(Request $request): Response
    {
        $search=$request->query->get('search');
        
        # Cases
        $cases = $this->getDoctrine()
        ->getRepository(Cases::class)
        ->findAny($search);
        
        # Case Items
        $caseitems = $this->getDoctrine()
        ->getRepository(CaseItems::class)
        ->findAny($search);

        # Events
        $events = $this->getDoctrine()
        ->getRepository(Event::class)
        ->findAny($search);
        
        # Invoices
        $invoices = $this->getDoctrine()
        ->getRepository(Invoice::class)
        ->findAny($search);
        
        # Staff
        $staff= $this->getDoctrine()
        ->getRepository(Staff::class)
        ->findAny($search);
        
        # Clients
        $clients = $this->getDoctrine()
        ->getRepository(Client::class)
        ->findAny($search);


        return $this->render('search/index.html.twig', [
            'search' => $search,
            'cases' => $cases,
            'caseitems' => $caseitems,
            'events' => $events,
            'invoices' => $invoices,
            'staffs' => $staff,
            'clients' => $clients,
        ]);
    }
}
