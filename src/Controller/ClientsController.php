<?php

namespace App\Controller;

use App\Entity\Client;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\ClientFormType;
use Symfony\Component\HttpFoundation\Request;

class ClientsController extends AbstractController
{
    /**
     * @Route("/client/{caseid}", name="client")
     */
    #[Route('/client/{caseid}', name: 'client')]
    public function show(string $caseid): Response
    {
        $user = $this->getUser();

        $clientname = explode ( " ",$caseid, 2);

        $client = $this->getDoctrine()
        ->getRepository(Client::class)
        ->findOneBy(['Fname'=>$clientname[0],'LName'=>$clientname[1]]);
        
        return $this->render('clients/show.html.twig', [
            'controller_name' => 'ClientsController',
            'client'=> $client
        ]);
    }
    /**
     * @Route("/client/{caseid}", name="clientShow")
     */
    #[Route('/client/{id}', name: 'clientShow')]
    public function showID(string $caseid): Response
    {
        $user = $this->getUser();

        $clientname = explode ( " ",$caseid, 2);

        $client = $this->getDoctrine()
        ->getRepository(Client::class)
        ->findOneBy(['Fname'=>$clientname[0],'LName'=>$clientname[1]]);
        
        return $this->render('clients/show.html.twig', [
            'controller_name' => 'ClientsController',
            'client'=> $client
        ]);
    }

    /**
     * @Route("/client/{id}/edit", name="clientEdit")
     */
    #[Route('/client/{id}/edit', name: 'clientEdit')]
    public function edit(string $id,Request $request): Response
    {
        $user = $this->getUser();
        $entityManager = $this->getDoctrine()->getManager();
        $client = $entityManager->getRepository(Client::class)->find($id);
        $form = $this->createForm(ClientFormType::class, $client);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $event = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($event);
            $entityManager->flush();

            return $this->redirectToRoute('clientShow',array('id' => $id));
        }
        else
        {
            return $this->render('clients/edit.html.twig', [
                'controller_name' => 'ClientsController',
                'case_form' => $form->createView(),
            ]);
        }
    }
    /**
     * @Route("/client/{id}/remove", name="clientRemove")
     */
    #[Route('/client/{id}/remove', name: 'clientRemove')]
    public function remove(string $id): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $event = $entityManager->getRepository(Client::class)->find($id);
        $entityManager->remove($event);
        $entityManager->flush();

        return $this->redirectToRoute('dashboard');
    }
}
