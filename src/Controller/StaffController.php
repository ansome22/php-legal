<?php

namespace App\Controller;

use App\Entity\Staff;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\StaffFormType;
use Symfony\Component\HttpFoundation\Request;

class StaffController extends AbstractController
{
    /**
     * @Route("/staffs", name="staffs")
     */
    #[Route('/staffs', name: 'staffs')]
    public function index(): Response
    {
        return $this->render('staff/index.html.twig', [
            'controller_name' => 'StaffController',
        ]);
    }
    /**
     * @Route("/staff/{id}", name="staff")
     */
    #[Route('/staff/{id}', name: 'staff')]
    public function show(string $id): Response
    {

        $staff = $this->getDoctrine()
        ->getRepository(Staff::class)
        ->findOneBy(['id'=>$id]);


        return $this->render('staff/index.html.twig', [
            'controller_name' => 'StaffController',
            'staff' => $staff
        ]);
    }
    /**
     * @Route("/staff/{id}/edit", name="editstaff")
     */
    #[Route('/staff/{id}/edit', name: 'editstaff')]
    public function edit(string $id,Request $request): Response
    {
        $user = $this->getUser();
        $event = $this->getDoctrine()
        ->getRepository(Staff::class)->find($id);
        $form = $this->createForm(StaffFormType::class, $event);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $event = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($event);
            $entityManager->flush();

            return $this->redirectToRoute('dashboard');
        }
        else
        {
            return $this->render('calendar/new.html.twig', [
                'controller_name' => 'CalendarController',
                'case_form' => $form->createView(),
            ]);
        }

    }
    /**
     * @Route("/staff/{id}/remove", name="removestaff")
     */
    #[Route('/staff/{id}/remove', name: 'removestaff')]
    public function remove(string $id): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $event = $entityManager->getRepository(Staff::class)->find($id);
        $entityManager->remove($event);
        $entityManager->flush();

        return $this->redirectToRoute('dashboard');
    }
}
