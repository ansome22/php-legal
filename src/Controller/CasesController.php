<?php

namespace App\Controller;

use App\Entity\Cases;
use App\Entity\Staff;
use App\Entity\Client;
use App\Entity\CaseItems;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\CasesFormType;
use App\Form\CasesItemsFormType;
use Twig\Environment;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\String\Slugger\SluggerInterface;
use App\Service\FileUploader;

class CasesController extends AbstractController
{
    /**
     * @Route("/cases", name="cases")
     */
    #[Route('/cases', name: 'cases')]
    public function index(): Response
    {
        $user = $this->getUser();
        $cases = $user->getStaff()->getCases();
        
        return $this->render('cases/index.html.twig', [
            'controller_name' => 'CasesController',
            'cases' => $cases,
        ]);
    }
    /**
     * @Route("/case/new", name="addcase")
     */
    #[Route('/case/new', name: 'addcase')]
    public function create(Request $request): Response
    {
        $user = $this->getUser();
        $case = new Cases();
        $form = $this->createForm(CasesFormType::class, $case);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $case = $form->getData();
            $case->setHashedID(bin2hex(random_bytes(20)));

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($case);
            $entityManager->flush();
            
            return $this->redirectToRoute('cases');
        }
        else {
            return $this->render('cases/new.html.twig', [
                'case_form' => $form->createView(),
            ]);
        }

    }
    /**
     * @Route("/case/{id}/new", name="newcaseitem")
     */
    #[Route('/case/{id}/new', name: 'newcaseitem')]
    public function newcaseitem(string $id, Request $request, FileUploader $fileUploader): Response
    {
        $user = $this->getUser();
        $case = new CaseItems();
        $form = $this->createForm(CasesItemsFormType::class, $case);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $case = $this->getDoctrine()
            ->getRepository(Cases::class)
            ->findOneBy(['HashedID'=>$id]);

            $caseitem = $form->getData();
            $caseitem->setCaseId($case);

            /** @var UploadedFile $brochureFile */
            $File = $form->get('File')->getData();
            // this condition is needed because the 'brochure' field is not required
            // so the PDF file must be processed only when a file is uploaded
            if ($File) {
                $FileName = $fileUploader->upload($File);
                $caseitem->setFile($FileName);
            }
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($caseitem);
            $entityManager->flush();
            
            return $this->redirectToRoute('case', array('id' => $id));
        }
        else
        {
            return $this->render('caseitems/add.html.twig', [
                'case_form' => $form->createView(),
            ]);
        }
    }
    /**
     * @Route("/case/{id}/edit", name="editcase")
     */
    #[Route('/case/{id}/edit', name: 'editcase')]
    public function edit(string $id): Response
    {
        $user = $this->getUser();
        $random = random_bytes(40);
        $case = $this->getDoctrine()
        ->getRepository(Cases::class)
        ->findOneBy(['HashedID'=>$id]);
        $form = $this->createForm(CasesFormType::class, $case);

        return $this->render('cases/add.html.twig', [
            'case_form' => $form->createView(),
        ]);
    }
    /**
     * @Route("/case/{id}/remove", name="removecase")
     */
    #[Route('/case/{id}/remove', name: 'removecase')]
    public function remove(string $id): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $event = $entityManager->getRepository(Cases::class)->findOneBy(['HashedID'=>$id]);
        $entityManager->remove($event);
        $entityManager->flush();

        return $this->redirectToRoute('cases');
    }
    /**
     * @Route("/case/{id}/item/{item}/remove", name="removecaseitem")
     */
    #[Route('/case/{id}/item/{item}/remove', name: 'removecaseitem')]
    public function removeitem(string $id,string $item): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $event = $entityManager->getRepository(CaseItems::class)->find($item);
        $entityManager->remove($event);
        $entityManager->flush();

        return $this->redirectToRoute('case', array('id' => $id));
    }
    /**
     * @Route("/case/{id}/item/{item}/edit", name="editcaseitem")
     */
    #[Route('/case/{id}/item/{item}/edit', name: 'editcaseitem')]
    public function edititem(string $id,string $item): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $event = $entityManager->getRepository(CaseItems::class)->find($item);
        $entityManager->remove($event);
        $entityManager->flush();

        return $this->redirectToRoute('case', array('id' => $id));
    }
    /**
     * @Route("/cases/{id}", name="case")
     */
    #[Route('/cases/{id}', name: 'case')]
    public function show(string $id): Response
    {
        $user = $this->getUser();
        $case = $this->getDoctrine()
            ->getRepository(Cases::class)
            ->findOneBy(['HashedID'=>$id]);

        $caseItems=$case->getcaseItems()->getValues();
        $invoices = $case->getInvoices();
        $events = $case->getEvents();
        $todos = $case->getTodos();

        return $this->render('cases/show.html.twig', [
            'case' => $case,
            'caseitems'=>$caseItems,
            'invoices' => $invoices,
            'events' => $events,
            'todos' => $todos
        ]);
    }
}
