<?php

namespace App\Entity;

use App\Repository\CasesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * @ORM\Entity(repositoryClass=CasesRepository::class)
 */
class Cases
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $HashedID;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $Name;

    /**
     * @ORM\ManyToOne(targetEntity=Client::class, inversedBy="cases")
     * @ORM\JoinColumn(nullable=false)
     */
    private $ClientID;

    /**
     * @ORM\OneToMany(targetEntity=Event::class, mappedBy="CaseID", orphanRemoval=true)
     */
    private $events;

    /**
     * @ORM\OneToMany(targetEntity=Todo::class, mappedBy="CaseID", orphanRemoval=true)
     */
    private $todos;

    /**
     * @ORM\ManyToMany(targetEntity=Staff::class, inversedBy="cases")
     */
    private $Staff;

    /**
     * @ORM\OneToMany(targetEntity=CaseItems::class, mappedBy="case_id", orphanRemoval=true)
     */
    private $caseItems;

    /**
     * @ORM\OneToMany(targetEntity=Invoice::class, mappedBy="cases")
     */
    private $invoices;

    public function __toString(): string
    {
        return $this->Name;
    }

    public function __construct()
    {
        $this->events = new ArrayCollection();
        $this->todos = new ArrayCollection();
        $this->Staff = new ArrayCollection();
        $this->caseItems = new ArrayCollection();
        $this->invoices = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getHashedID(): ?string
    {
        return $this->HashedID;
    }

    public function setHashedID(string $HashedID): self
    {
        $this->HashedID = $HashedID;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->Name;
    }

    public function setName(string $Name): self
    {
        $this->Name = $Name;

        return $this;
    }

    public function getClientID(): ?Client
    {
        return $this->ClientID;
    }

    public function setClientID(?Client $ClientID): self
    {
        $this->ClientID = $ClientID;

        return $this;
    }

    /**
     * @return Collection|Event[]
     */
    public function getEvents(): Collection
    {
        return $this->events;
    }

    public function addEvent(Event $event): self
    {
        if (!$this->events->contains($event)) {
            $this->events[] = $event;
            $event->setCaseID($this);
        }

        return $this;
    }

    public function removeEvent(Event $event): self
    {
        if ($this->events->removeElement($event)) {
            // set the owning side to null (unless already changed)
            if ($event->getCaseID() === $this) {
                $event->setCaseID(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Todo[]
     */
    public function getTodos(): Collection
    {
        return $this->todos;
    }

    public function addTodo(Todo $todo): self
    {
        if (!$this->todos->contains($todo)) {
            $this->todos[] = $todo;
            $todo->setCaseID($this);
        }

        return $this;
    }

    public function removeTodo(Todo $todo): self
    {
        if ($this->todos->removeElement($todo)) {
            // set the owning side to null (unless already changed)
            if ($todo->getCaseID() === $this) {
                $todo->setCaseID(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Staff[]
     */
    public function getStaff(): Collection
    {
        return $this->Staff;
    }

    public function addStaff(Staff $staff): self
    {
        if (!$this->Staff->contains($staff)) {
            $this->Staff[] = $staff;
        }

        return $this;
    }

    public function removeStaff(Staff $staff): self
    {
        $this->Staff->removeElement($staff);

        return $this;
    }

    /**
     * @return Collection|CaseItems[]
     */
    public function getCaseItems(): Collection
    {
        return $this->caseItems;
    }

    public function addCaseItem(CaseItems $caseItem): self
    {
        if (!$this->caseItems->contains($caseItem)) {
            $this->caseItems[] = $caseItem;
            $caseItem->setCaseId($this);
        }

        return $this;
    }

    public function removeCaseItem(CaseItems $caseItem): self
    {
        if ($this->caseItems->removeElement($caseItem)) {
            // set the owning side to null (unless already changed)
            if ($caseItem->getCaseId() === $this) {
                $caseItem->setCaseId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Invoice[]
     */
    public function getInvoices(): Collection
    {
        return $this->invoices;
    }

    public function addInvoice(Invoice $invoice): self
    {
        if (!$this->invoices->contains($invoice)) {
            $this->invoices[] = $invoice;
            $invoice->setCases($this);
        }

        return $this;
    }

    public function removeInvoice(Invoice $invoice): self
    {
        if ($this->invoices->removeElement($invoice)) {
            // set the owning side to null (unless already changed)
            if ($invoice->getCases() === $this) {
                $invoice->setCases(null);
            }
        }

        return $this;
    }
}
