<?php

namespace App\Entity;

use App\Repository\StaffRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=StaffRepository::class)
 */
class Staff
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Fname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Lname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Address;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Phone;


    /**
     * @ORM\OneToMany(targetEntity=Todo::class, mappedBy="CaseID", orphanRemoval=true)
     */
    private $todos;

    /**
     * @ORM\ManyToMany(targetEntity=Cases::class, mappedBy="Staff")
     */
    private $cases;

    /**
     * @ORM\OneToMany(targetEntity=Invoice::class, mappedBy="BilledStaffID", orphanRemoval=true)
     */
    private $invoices;

    /**
     * @ORM\OneToOne(targetEntity=User::class, mappedBy="staff", cascade={"persist", "remove"})
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity=Template::class, mappedBy="staff")
     */
    private $templates;

    public function __toString(): string
    {
        return $this->Fname. ' ' .$this->Lname;
    }

    public function __construct()
    {
        $this->todos = new ArrayCollection();
        $this->cases = new ArrayCollection();
        $this->invoices = new ArrayCollection();
        $this->caseTypes = new ArrayCollection();
        $this->templates = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFname(): ?string
    {
        return $this->Fname;
    }

    public function setFname(string $Fname): self
    {
        $this->Fname = $Fname;

        return $this;
    }

    public function getLname(): ?string
    {
        return $this->Lname;
    }

    public function setLname(string $Lname): self
    {
        $this->Lname = $Lname;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->Address;
    }

    public function setAddress(string $Address): self
    {
        $this->Address = $Address;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->Phone;
    }

    public function setPhone(string $Phone): self
    {
        $this->Phone = $Phone;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->Email;
    }

    public function setEmail(string $Email): self
    {
        $this->Email = $Email;

        return $this;
    }

    /**
     * @return Collection|Todo[]
     */
    public function getTodos(): Collection
    {
        return $this->todos;
    }

    public function addTodo(Todo $todo): self
    {
        if (!$this->todos->contains($todo)) {
            $this->todos[] = $todo;
        }

        return $this;
    }

    public function removeTodo(Todo $todo): self
    {
        if ($this->todos->removeElement($todo)) {
            // set the owning side to null (unless already changed)
            $todo->removeTodo($this);
        }

        return $this;
    }

    /**
     * @return Collection|Cases[]
     */
    public function getCases(): Collection
    {
        return $this->cases;
    }

    public function addCase(Cases $case): self
    {
        if (!$this->cases->contains($case)) {
            $this->cases[] = $case;
            $case->addStaff($this);
        }

        return $this;
    }

    public function removeCase(Cases $case): self
    {
        if ($this->cases->removeElement($case)) {
            $case->removeStaff($this);
        }

        return $this;
    }

    /**
     * @return Collection|Invoice[]
     */
    public function getInvoices(): Collection
    {
        return $this->invoices;
    }

    public function addInvoice(Invoice $invoice): self
    {
        if (!$this->invoices->contains($invoice)) {
            $this->invoices[] = $invoice;
            $invoice->setBilledStaffID($this);
        }

        return $this;
    }

    public function removeInvoice(Invoice $invoice): self
    {
        if ($this->invoices->removeElement($invoice)) {
            // set the owning side to null (unless already changed)
            if ($invoice->getBilledStaffID() === $this) {
                $invoice->setBilledStaffID(null);
            }
        }

        return $this;
    }


    public function getUser(): ?user
    {
        return $this->user;
    }

    public function setUser(?user $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|Template[]
     */
    public function getTemplate(): Collection
    {
        return $this->templates;
    }

    public function addTemplate(Template $template): self
    {
        if (!$this->template->contains($template)) {
            $this->template[] = $template;
            $template->setStaff($this);
        }

        return $this;
    }

    public function removeTemplate(Template $template): self
    {
        if ($this->template->removeElement($template)) {
            // set the owning side to null (unless already changed)
            if ($template->getStaff() === $this) {
                $template->setStaff(null);
            }
        }

        return $this;
    }
}
