<?php

namespace App\Entity;

use App\Repository\CaseItemsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CaseItemsRepository::class)
 */
class CaseItems
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Cases::class, inversedBy="caseItems")
     * @ORM\JoinColumn(nullable=false)
     */
    private $case_id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $File;

    /**
     * @ORM\Column(type="array")
     */
    private $CaseType = [];

    public function __construct()
    {
        $this->caseTypes = new ArrayCollection();
        $this->CaseTypes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCaseId(): ?Cases
    {
        return $this->case_id;
    }

    public function setCaseId(?Cases $case_id): self
    {
        $this->case_id = $case_id;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->Name;
    }

    public function setName(string $Name): self
    {
        $this->Name = $Name;

        return $this;
    }

    public function getFile(): ?string
    {
        return $this->File;
    }

    public function setFile(string $File): self
    {
        $this->File = $File;

        return $this;
    }

    public function getCaseType(): ?array
    {
        return $this->CaseType;
    }

    public function setCaseType(array $CaseType): self
    {
        $this->CaseType = $CaseType;

        return $this;
    }
}
