<?php

namespace App\Entity;

use App\Repository\InvoiceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=InvoiceRepository::class)
 */
class Invoice
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Name;

    /**
     * @ORM\ManyToOne(targetEntity=Staff::class, inversedBy="invoices")
     * @ORM\JoinColumn(nullable=false)
     */
    private $BilledStaffID;

    /**
     * @ORM\Column(type="date")
     */
    private $DateOfIssue;

    /**
     * @ORM\OneToMany(targetEntity=InvoiceItems::class, mappedBy="InvoiceID", orphanRemoval=true)
     */
    private $invoiceItems;

    /**
     * @ORM\Column(type="boolean")
     */
    private $Paid;

    /**
     * @ORM\ManyToOne(targetEntity=Cases::class, inversedBy="invoices")
     */
    private $cases;

    public function __toString(): string
    {
        return $this->Name;
    }

    public function __construct()
    {
        $this->invoiceItems = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->Name;
    }

    public function setName(string $Name): self
    {
        $this->Name = $Name;

        return $this;
    }

    public function getBilledStaffID(): ?Staff
    {
        return $this->BilledStaffID;
    }

    public function setBilledStaffID(?Staff $BilledStaffID): self
    {
        $this->BilledStaffID = $BilledStaffID;

        return $this;
    }

    public function getDateOfIssue(): ?\DateTimeInterface
    {
        return $this->DateOfIssue;
    }

    public function setDateOfIssue(\DateTimeInterface $DateOfIssue): self
    {
        $this->DateOfIssue = $DateOfIssue;

        return $this;
    }

    /**
     * @return Collection|InvoiceItems[]
     */
    public function getInvoiceItems(): Collection
    {
        return $this->invoiceItems;
    }

    public function addInvoiceItem(InvoiceItems $invoiceItem): self
    {
        if (!$this->invoiceItems->contains($invoiceItem)) {
            $this->invoiceItems[] = $invoiceItem;
            $invoiceItem->setInvoiceID($this);
        }

        return $this;
    }

    public function removeInvoiceItem(InvoiceItems $invoiceItem): self
    {
        if ($this->invoiceItems->removeElement($invoiceItem)) {
            // set the owning side to null (unless already changed)
            if ($invoiceItem->getInvoiceID() === $this) {
                $invoiceItem->setInvoiceID(null);
            }
        }

        return $this;
    }

    public function getPaid(): ?bool
    {
        return $this->Paid;
    }

    public function setPaid(bool $Paid): self
    {
        $this->Paid = $Paid;

        return $this;
    }

    public function getCases(): ?Cases
    {
        return $this->cases;
    }

    public function setCases(?Cases $cases): self
    {
        $this->cases = $cases;

        return $this;
    }
}
